tcweathertray
===================

A system tray icon that shows the weather for a given city.
Weather informations are retrieved from [Weatherbit](https://www.weatherbit.io/).

Tray icons are from Weatherbit.

![Screenshot](https://abload.de/img/screenw6s1h.png)

Requirements:
-------------
- IUP
- IM
- JSON-C
- CURL

How to compile:
---------------

`cc -I/usr/include/json-c/ -lim -liup -liupim -liupimglib -lcurl -ljson-c main.c`

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png) 

[Note: I am no longer working on this.]