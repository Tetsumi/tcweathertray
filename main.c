/*
  tcweathertray

  Contributors:
     Tetsumi <tetsumi@protonmail.com>

  Copyright (C) 2018 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <iup.h>
#include <iupim.h>
#include <curl/curl.h>
#include <json.h>
#include <stdbool.h>
#include <sys/time.h>

typedef struct 
{
	char *bytes;
	size_t size;
} Buffer;

Buffer g_buffer;
CURL *g_curlH;
char g_apiKey[513];
char g_trayTip[1024];
char g_url[1112];
char g_city[513];
Ihandle *g_apiKeyField;
Ihandle *g_cityField;
Ihandle *g_dialog;

void  updateUrl(void);
void setupGui(void);
size_t curlCallback(void *contents, size_t size, size_t nmemb, void *userp);
bool getUrl(char *url, Buffer *buffer);
void initializeCurl(void);
void finalizeCurl(void);
int refreshTray(Ihandle *h);
int trayClick(Ihandle *ih, int button, int pressed, int dclick);

int refreshTray(Ihandle *h)
{
	// Retrieve json
	if('\0' == g_url[0] || !getUrl(g_url, &g_buffer))
	{
		IupSetAttribute(g_dialog, "TRAYIMAGE", "IUP_MessageError");
		return IUP_DEFAULT;
	}
	
	json_object *jsonObj = json_tokener_parse(g_buffer.bytes);
	json_object *data = json_object_array_get_idx(
		json_object_object_get(jsonObj, "data"),
		0);
	json_object *weather = json_object_object_get(data, "weather");

	// update tray tip
	sprintf(g_trayTip,
	        "%s\n%.1f °C\n%s",
		json_object_get_string(json_object_object_get(data,
							      "city_name")),
		json_object_get_double(json_object_object_get(data,
							      "temp")),
		json_object_get_string(json_object_object_get(weather,
							      "description")));
	IupSetAttribute(g_dialog, "TRAYTIP", g_trayTip);

	char const *icon = json_object_get_string(
		json_object_object_get(weather, "icon"));
	Ihandle *iconImg = IupGetHandle(icon);
	
	if (!iconImg)
	{
		// update tray icon
		char fName[45] = {[44] = '\0'};
		sprintf(fName,
			"./res/%s.png",
			icon);
		IupSetHandle(icon, IupLoadImage(fName));
	}
	IupSetAttribute(g_dialog, "TRAYIMAGE", icon);
	json_object_put(jsonObj);
	return IUP_DEFAULT;
}

void updateUrl(void)
{
	if (1 >= strlen(g_city) || 30 > strlen(g_apiKey))
	{
		IupSetAttribute (g_dialog, "TRAYIMAGE", "IUP_MessageError");
		return;
	}
	sprintf(g_url,
	        "https://api.weatherbit.io/v2.0/current?city=%s&key=%s",
		g_city,
		g_apiKey);
        refreshTray(NULL);
}

int setupKeyCity(Ihandle* ih)
{
	int bc = IupGetParam("WeatherTray",
			     NULL,
			     NULL,
			     "Api key: %s\n"
			     "City: %s\n",
			     g_apiKey,
			     g_city);
	if (bc)
		updateUrl();
	return IUP_DEFAULT;
}

int closeCb(Ihandle* ih)
{
	return IUP_CLOSE;
}

int quitFromMenu(Ihandle* ih)
{
	Ihandle *timer = IupTimer();
	IupSetCallback(timer, "ACTION_CB", closeCb);
	IupSetAttributes(timer, "TIME=500, RUN=YES");
	return IUP_DEFAULT;
}

int trayClick(Ihandle *ih, int button, int pressed, int dclick)
{
	Ihandle* menu = IupMenu(IupItem("Configure", "configure"),
				IupItem("Quit", "quit"),
				NULL);
	IupSetAttribute(menu, "_DIALOG", (char*)ih);
	IupSetFunction("configure", (Icallback) setupKeyCity);
	IupSetFunction("quit", (Icallback)quitFromMenu);
	IupPopup(menu, IUP_MOUSEPOS, IUP_MOUSEPOS);
	IupDestroy(menu);
	return IUP_DEFAULT;
}

void setupGui(void)
{	
	g_dialog = IupDialog(NULL);	
	IupSetAttributes(g_dialog,
			 "TITLE=Weather Tray,"
			 "TRAYIMAGE=IUP_MessageError,"
			 "TRAYTIP=MISCONFIGURED,"
			 "TRAY=YES");	
	IupShowXY(g_dialog, IUP_CENTER, IUP_CENTER);
	IupSetAttribute(g_dialog, "HIDETASKBAR", "YES");
	IupSetCallback(g_dialog, "TRAYCLICK_CB", (Icallback)trayClick);	
	// Refresh the tray every two minutes
	Ihandle *timer = IupTimer();
	IupSetCallback(timer, "ACTION_CB", refreshTray);
	IupSetAttributes(timer, "TIME=120000, RUN=YES");
		    
}

size_t curlCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	size_t realSize = size * nmemb;
        Buffer *b = userp;
	size_t psize = b->size;
	b->size += realSize;
	b->bytes = realloc(b->bytes, b->size + 1);
	memcpy(b->bytes + psize, contents, realSize);
	b->bytes[b->size] = '\0';
	return realSize;
}

bool getUrl(char *url, Buffer *buffer)
{
	buffer->size = 0;
	curl_easy_setopt(g_curlH, CURLOPT_URL, url);
	curl_easy_setopt(g_curlH, CURLOPT_WRITEFUNCTION, curlCallback);
	curl_easy_setopt(g_curlH, CURLOPT_WRITEDATA, buffer);
        if (curl_easy_perform(g_curlH) != CURLE_OK)
		return false;
	long rCode = 0;
	curl_easy_getinfo(g_curlH, CURLINFO_RESPONSE_CODE, &rCode);
	return rCode == 200;
}

void initializeCurl(void)
{
        curl_global_init(CURL_GLOBAL_ALL);
	g_curlH = curl_easy_init();
	curl_easy_setopt(g_curlH, CURLOPT_USERAGENT, "libcurl-agent/1.0");	
}

void finalizeCurl(void)
{
	if (g_curlH)
	{
		curl_easy_cleanup(g_curlH);
		g_curlH = NULL;
	}
	curl_global_cleanup();
}

int main(int argc, char* argv[])
{
	IupOpen(&argc, &argv);
	IupImageLibOpen();
	IupSetGlobal("SINGLEINSTANCE", "tcweathertray");
	if (!IupGetGlobal("SINGLEINSTANCE"))
	{
		IupClose();  
		return EXIT_SUCCESS;
	}
	initializeCurl();
	setupGui();
	IupMainLoop();
	finalizeCurl();
	IupClose();
	puts("Bye.");
	return EXIT_SUCCESS;
}
